import { ChangeEvent } from "react";
import { ToDoEntryProps } from "./ToDoEntry";
  
export default function ToDoCheckBox({
    toDo: { id, Name, Done },
    updateToDoDoneStatus,
  }: ToDoEntryProps): JSX.Element {
    return (
        <input type={"checkbox"} className={"todo-checkbox"} id={`${id}`} checked={Done} onChange={(e: ChangeEvent<HTMLInputElement>) => updateToDoDoneStatus(id, e.target.checked)
        } />
    );
  }
  