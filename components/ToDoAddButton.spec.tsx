import {
    fireEvent,
    getByText,
    render,
    RenderResult,
} from "@testing-library/react";
import { ToDoAddButton } from "./ToDoAddButton";

describe("ToDoAddButton", () => {
    let renderResult: RenderResult;
    let button: HTMLButtonElement;
    let addToDo: (Name: string, Done: boolean) => void;

    beforeEach(() => {
        addToDo = jest.fn();
        renderResult = render(
            <ToDoAddButton addToDo={addToDo} />
        );
        button = renderResult.container.querySelector(
            "button"
        ) as HTMLButtonElement;
    });

    it("renders a button", () => {
        const { getByText, container } = renderResult;
        expect(getByText("+")).toBeInTheDocument();
        expect(container.querySelector("button")).toBeTruthy();
    });

    it("renders/opens a modal", () => {
        fireEvent.click(button);
        const { getByText, container } = renderResult;
        expect(getByText("Add New ToDo")).toBeInTheDocument();
    });

    it("creates a ToDo", () => {
        fireEvent.click(button);
        const { getByRole, getByText, container } = renderResult;
        fireEvent.change(getByRole("textbox"), {target: { value: "new ToDo"}});
        fireEvent.click(getByText("Add"));
        expect(addToDo).toHaveBeenCalledWith("new ToDo", false);
    });

    it("discards creation of a ToDo", () => {
        fireEvent.click(button);
        const { getByRole, getByText, container } = renderResult;
        const inputToDo = getByRole("textbox");
        fireEvent.change(inputToDo, {target: { value: "new ToDo"}});
        fireEvent.click(getByText("Discard"));
        expect(inputToDo).not.toBeInTheDocument();
    });

    it("closes modal", () => {
        fireEvent.click(button);
        const { getByRole, getByText, container } = renderResult;
        const inputToDo = getByRole("textbox");
        expect(inputToDo).toBeInTheDocument();
        fireEvent.click(getByText("×"));
        expect(inputToDo).not.toBeInTheDocument();
    });
});
