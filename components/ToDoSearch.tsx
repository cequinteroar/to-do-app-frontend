import { ChangeEvent } from "react";

export interface ToDoSearchProps {
    toDoName: string;
    searchName: (text: string) => void;
}

export function ToDoSearch({toDoName, searchName}: ToDoSearchProps): JSX.Element {
  return (
    <input className="todo-search" type={"text"} placeholder={"Search by name"} value={toDoName} onChange={(evt: ChangeEvent<HTMLInputElement>) => searchName(evt.currentTarget.value)} />
  );
}