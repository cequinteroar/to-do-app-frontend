import { ToDo } from "../types/ToDo";

export interface ToDoApi {
  getAllToDos(): Promise<ToDo[]>;
  updateToDo(id: number, toDo: ToDo): Promise<void>;
  createToDo(name: string, done: boolean): Promise<ToDo>;
}
