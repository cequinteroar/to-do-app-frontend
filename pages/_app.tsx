import "../styles/globals.css";

import type { AppProps } from "next/app";
import styled from "styled-components";

const AppFrame = styled.div`
  width: 80%;
  margin: auto;
`;

function MyApp({ Component, pageProps }: AppProps) {
  return (
    <AppFrame>
      <Component {...pageProps} />
    </AppFrame>
  );
}

export default MyApp;
