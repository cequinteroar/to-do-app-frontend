import {
    fireEvent,
    render,
    RenderResult,
} from "@testing-library/react";
import { ToDoFeaturesBar } from "./ToDoFeaturesBar";

describe("ToDoFeaturesBar", () => {
    let renderResult: RenderResult;
    let div: HTMLDivElement;
    let searchName: (text: string) => void;
    let addToDo: (Name: string, Done: boolean) => void;
    let filteredByDone: () => void;
    let toDoName: string;

    beforeEach(() => {
        searchName = jest.fn();
        filteredByDone = jest.fn();
        addToDo = jest.fn();
        renderResult = render(
            <ToDoFeaturesBar addToDo={addToDo} searchName={searchName} toDoName={toDoName} filteredByDone={filteredByDone} />
        );
        div = renderResult.container.querySelector(
            "div.todo-horizontalBar"
        ) as HTMLDivElement;
    });

    it("renders features bar", () => {
        const { getByText, container } = renderResult;
        const featuresTitle = getByText("To-Dos");
        expect(featuresTitle).toBeInTheDocument();
        expect(container.querySelector("div")).toBeTruthy();
    });

    it("Filter by done", () => {
        const { getByRole, getByText, container } = renderResult;
        const showDone = getByRole("checkbox");
        fireEvent.click(showDone);
        expect(filteredByDone).toHaveBeenCalled();
    });
});
