This is a [Next.js](https://nextjs.org/) project bootstrapped with [`create-next-app`](https://github.com/vercel/next.js/tree/canary/packages/create-next-app).

## Before Starting
Be careful! Due to usage of a port from razer synapsis, this project reads the backend utilities from [http://localhost:1338] (Port 1338) (see `api/ToDoApiImpl.ts`).
Please either change the port of your backend code running in local or change here the port in the corresponding file.

## Getting Started

First, run the development server:

```bash
npm run dev
# or
yarn dev
```

Open [http://localhost:3000](http://localhost:3000) with your browser to see the result.

You can start editing the page by modifying `pages/index.tsx`. The page auto-updates as you edit the file.

[API routes](https://nextjs.org/docs/api-routes/introduction) can be accessed on [http://localhost:3000/api/hello](http://localhost:3000/api/hello). This endpoint can be edited in `pages/api/hello.ts`.

The `pages/api` directory is mapped to `/api/*`. Files in this directory are treated as [API routes](https://nextjs.org/docs/api-routes/introduction) instead of React pages.

## What was considered?
For creating the user interface for this project, some ux and usability readings were done in order to know which is the current status for ToDo List GUIs. The links are:
[https://medium.muz.li/designing-pocket-lists-18b6cafd1161?gi=24974c2aca0b] [https://uxplanet.org/my-ux-wishlist-for-to-do-list-apps-d43f737817bd]
Also, some basics about human-centered design were applied since in each of the interactions you'll find in the project, you'll have an immediate visual feedback of your actions.
Color selection was based on the link exposed in here and used in a simplistic way. Green is used for positive selections, red for negative and gray as a disabled color. An extra light gray was used for highlighting the current ToDo, which the user is interacting with.
Finally, the measurement unit used for most of the styles was "rem" because it takes the size of the root element, normally 16px but it can be different for some browsers (it ensures the consistency of the font size and spacing of each element in the GUI as well as improves the usage and ux of the app in several browsers).

## Tests
Test were modified according to the components created, the tests were done regarding the utilities added, e.g. there are tests checking if the user types something in the search text input, the corresponding filter function will be triggered. Considering the last explanation, three new test suites were created and are green (pipeline requirements).

## Future Work
1. Implement a feature to delete ToDos.
2. Splitting styles code in different files considering one file per component.
3. Improve the responsive design: there is still some improvement spaces for the app regarding some screen resolutions.
4. Include user management to allow only ToDo's creation for logged in users.

## Deploy on Vercel

The easiest way to deploy your Next.js app is to use the [Vercel Platform](https://vercel.com/new?utm_medium=default-template&filter=next.js&utm_source=create-next-app&utm_campaign=create-next-app-readme) from the creators of Next.js.

Check out our [Next.js deployment documentation](https://nextjs.org/docs/deployment) for more details.
