import styled from "styled-components";
import { ToDo } from "../types/ToDo";
import ToDoCheckBox from "./ToDoCheckBox";
import classNames from "classnames";

const ToDoWrapper = styled.div`
  border: 0;
  width: 70%;
  padding: 1.5rem;
  background: transparent;
  box-shadow: black 0px 1px 0px;
  margin: 0.125rem auto;
  display: flex;
  flex-direction: row;
  justify-content: flex-start;
  align-items: center;
`;

export interface ToDoEntryProps {
  toDo: ToDo;
  updateToDoDoneStatus: (id: number, done: boolean) => void;
}

export const ToDoEntry = ({
  toDo: { id, Name, Done },
  updateToDoDoneStatus,
}: ToDoEntryProps): JSX.Element => {

  const props = {
    toDo: { id, Name, Done },
    updateToDoDoneStatus
  };

  const labelClassnames = classNames('checkLabel',{'isDone': Done});

  return (
    <ToDoWrapper className="todo-wrapper">
      <ToDoCheckBox {...props} />
      <label className={labelClassnames} htmlFor={`${id}`}>{Name}</label>
    </ToDoWrapper>
  );
}
