import { ToDo } from "../types/ToDo";

export const mockToDos: ToDo[] = [
  { id: 0, Name: "Wash guinea pig", Done: false },
  { id: 1, Name: "Bring out trash", Done: true },
];
