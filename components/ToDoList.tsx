import React from "react";
import styled from "styled-components";
import { ToDo } from "../types/ToDo";
import {ToDoEntry} from "./ToDoEntry";
import { ToDoFeaturesBar } from "./ToDoFeaturesBar";

const ToDoListWrapper = styled.div`
  margin: 0 auto;
  width: 100%;
`;

export interface ToDoListProps {
  toDos: ToDo[];  
  updateToDoDoneStatus: (id: number, done: boolean) => void;
  addToDo: (Name: string, Done: boolean) => void;
}

export function ToDoList({toDos, updateToDoDoneStatus, addToDo}: ToDoListProps): JSX.Element {
  const [onlyDone, setOnlyDone] = React.useState<boolean>(false);
  const [name, setName] = React.useState<string>("");
  //  Function to get filtered ToDos if done and/or with name
  const filteredToDos: ToDo[] = toDos.filter((toDo) => {
    if(name !== "" && name !== null){
      return onlyDone ? toDo.Done && toDo.Name?.includes(name) : toDo.Name?.includes(name);
    }else {
      return toDo.Done;
    }
  });
  //  Filter State Functions
  const changeFilter = () => {
    const filter = !onlyDone;
    setOnlyDone(filter);
  }
  const changeName = (text: string) => {
    setName(text);
  }
  //  Filtered ToDos
  const toShow: ToDo[] = onlyDone || (name !== "" && name !== null) ? filteredToDos : toDos.sort((t1, t2) => +(t1.Done || false) - +(t2.Done || false));


  const featuresProps = {toDos, filteredByDone: changeFilter, toDoName: name, searchName: changeName, addToDo: addToDo }
  return (
    <ToDoListWrapper>
      <ToDoFeaturesBar {...featuresProps} />
      {toShow.map((toDo) => (
          <ToDoEntry
            key={toDo.id}
            toDo={toDo}
            updateToDoDoneStatus={updateToDoDoneStatus}
          />
        ))}
    </ToDoListWrapper>
  );
}
