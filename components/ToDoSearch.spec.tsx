import {
    fireEvent,
    render,
    RenderResult,
} from "@testing-library/react";
import { ToDoSearch } from "./ToDoSearch";

describe("ToDoSearch", () => {
    let renderResult: RenderResult;
    let input: HTMLInputElement;
    let searchName: (text: string) => void;
    let toDoName: string;

    beforeEach(() => {
        searchName = jest.fn();
        renderResult = render(
            <ToDoSearch searchName={searchName} toDoName={toDoName} />
        );
        input = renderResult.container.querySelector(
            "input"
        ) as HTMLInputElement;
    });

    it("renders a search name input", () => {
        const { getByRole, container } = renderResult;
        const inputsearch = getByRole("textbox");
        expect(inputsearch).toBeInTheDocument();
        expect(container.querySelector("input")).toBeTruthy();
    });

    it("searches a ToDo", () => {
        fireEvent.click(input);
        const { getByRole, getByText, container } = renderResult;
        const inputsearch = getByRole("textbox");
        fireEvent.change(inputsearch, {target: { value: "test"}});
        expect(searchName).toHaveBeenCalledWith("test");
    });
});
