import { ToDoApi } from "./ToDoApi";
import { ToDo } from "../types/ToDo";

export class ToDoApiImpl implements ToDoApi {
  constructor(
    private BACKEND_URL = process.env.NEXT_PUBLIC_BACKEND_URL ||
      "http://localhost:1338",
    private TO_DO_ENDPOINT = "/to-dos"
  ) {}

  getAllToDos(): Promise<ToDo[]> {
    return fetch(`${this.BACKEND_URL}${this.TO_DO_ENDPOINT}`).then((result) =>
      result.json()
    );
  }

  updateToDo(id: number, toDo: ToDo): Promise<void> {
    return fetch(`${this.BACKEND_URL}${this.TO_DO_ENDPOINT}/${id}`, {
      method: "PUT",
      body: JSON.stringify(toDo),
      headers: {
        "Content-Type": "application/json",
      },
    }).then(() => {});
  }

  createToDo(name:string, done:boolean): Promise<ToDo> {
    return fetch(`${this.BACKEND_URL}${this.TO_DO_ENDPOINT}`, {
      method: "POST",
      body: JSON.stringify({Name: name, Done: done}),
      headers: {
        "Content-Type": "application/json",
      },
    }).then((response) => response.json());
  }
}
