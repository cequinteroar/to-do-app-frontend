import styled from "styled-components";
import { ToDoSearch } from "./ToDoSearch";
import { ToDoAddButton } from "./ToDoAddButton";

const StyledLabel = styled.label`
  margin-right: 0.313rem;
  color: rgb(16, 147, 255);
  display: flex;
  align-items: center;
`;

export interface ToDoFeatres {
    filteredByDone: () => void;
    toDoName: string;
    searchName: (text: string) => void;
    addToDo: (Name: string, Done: boolean) => void;
}

export function ToDoFeaturesBar({ filteredByDone, toDoName, addToDo, searchName }: ToDoFeatres): JSX.Element {
    return (
        <div className="todo-horizontalBar">
            <h1 className="todo-title">To-Dos</h1>
            <div className="todo-functions">
                <ToDoSearch {...{toDoName, searchName}}></ToDoSearch>
                <StyledLabel>Show only Done: </StyledLabel>
                <label className="switch">
                    <input type="checkbox" onClick={filteredByDone} />
                    <span className="slider round"></span>
                </label>
                <ToDoAddButton addToDo={addToDo}></ToDoAddButton>
            </div>
        </div>
    );
}