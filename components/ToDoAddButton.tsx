import React from "react";
import { ChangeEvent } from "react";
import classNames from "classnames";

export interface ToDoButtonProps {
  addToDo: (Name: string, Done: boolean) => void;
}

export function ToDoAddButton({ addToDo }: ToDoButtonProps): JSX.Element {
  const [showAddModal, setShowAddModal] = React.useState<boolean>(false);
  const [todoName, setToDoName] = React.useState<string>("");
  const [todoDone, setToDoDone] = React.useState<boolean>(false);
  const done = todoDone;

  //  Classnames
  const inputAddClassNames = classNames("todo-search todo-inputName");
  const checkboxAddClassNames = classNames("todo-checkbox todo-inputDone");

  //  Button Function
  const resetValues = () => {
    setToDoDone(false);
    setToDoName("");
  }
  const add = () => {
    addToDo(todoName, todoDone);
    setShowAddModal(false);
    resetValues();
  };
  const close = () => {
    setShowAddModal(false)
    resetValues();
  };
  
  return (<>
    <button className="todo-addButton" onClick={() => {
      setShowAddModal(true);
    }}>+</button>
    {showAddModal && <div className="todo-addModal">
      <div className="modal-content">
        <div className="todo-horizontalBar modal-header">
          <h3>Add New ToDo</h3>
          <span className="close" onClick={() => close()}>&times;</span>
        </div>
        <div className="modal-body">
          <input className={inputAddClassNames} type={"text"} placeholder={"ToDo description"} value={todoName} onChange={(evt: ChangeEvent<HTMLInputElement>) => setToDoName(evt.currentTarget.value)} />
          <input type={"checkbox"} className={checkboxAddClassNames} checked={todoDone} onChange={() => setToDoDone(!done)} />
        </div>
        <div className="modal-buttons">
          <button className="addButtons todo-discard" onClick={() => close()}>Discard</button>
          <button className="addButtons todo-add" onClick={() => add()}>Add</button>
        </div>
      </div>

    </div>
    }
  </>
  );
}